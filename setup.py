from distutils.core import setup

setup(
    name='DeepProbLog',
    version='0.1dev',
    packages=['deepproblog',],
    long_description=open('README.md').read(),
    install_requires=['torch', 'torchvision', 'problog', 'PySDD']
)
