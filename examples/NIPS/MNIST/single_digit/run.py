from deepproblog.train import train_model
from deepproblog.data_loader import load
from examples.NIPS.MNIST.mnist import test_MNIST, MNIST_Net, neural_predicate
from deepproblog.model import Model
from deepproblog.optimizer import Optimizer
from deepproblog.network import Network
import torch
import os

FILE_PATH = os.path.dirname(os.path.realpath(__file__))

queries = load(os.path.join(FILE_PATH, 'train_data.txt'))

with open(os.path.join(FILE_PATH, 'addition.pl')) as f:
    problog_string = f.read()


network = MNIST_Net()
net = Network(network, 'mnist_net', neural_predicate)
net.optimizer = torch.optim.Adam(network.parameters(),lr = 0.001)
model = Model(problog_string, [net], caching=False)
optimizer = Optimizer(model, 2)

train_model(model,queries, 1, optimizer,test_iter=1000,test=test_MNIST,snapshot_iter=10000)
